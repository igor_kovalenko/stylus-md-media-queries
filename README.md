# stylus-md-media-queries

Stylus mixins to quickly write media queries for google material design

## Install

With [npm](http://npmjs.org) do:

```bash
$ npm install stylus-md-media-queries --save
```

## Usage

```styl
@import "../node_modules/stylus-md-media-queries/index.styl";

.some-class
    /***** Portrait
    /* small handset portrait */
    +max-screen-portrait(360)
        background-color #09bcd4

    /* medium handset portrait */
    +screen-portrait(361, 400)
        background-color #00acc1

    /* large handset portrait */
    +screen-portrait(401, 600)
        background-color #0097a7

    /* small tablet portrait */
    +screen-portrait(601, 720)
        background-color #38a4dd

    /* large tablet portrait */
    +screen-portrait(721, 840)
        background-color #1b87c9

    /***** Landscape
    /* small handset landscape */
    +screen-landscape(481, 600)
        background-color #09bcd4

    /* medium handset landscape */
    +screen-landscape(601, 720)
        background-color #09bcd4

    /* large handset landscape */
    +screen-landscape(721, 960)
        background-color #00acc1

    /* small tablet landscape */
    +screen-landscape(961, 1024)
        background-color #09bcd4

    /* large tablet landscape */
    +screen-landscape(1025, 1280)
        background-color #1b87c9

    /***** window
    /* xsmall */
    +max-screen(600)
        background-color #7b86c2

    /* small */
    +screen(601, 960)
        background-color #5e6bb2

    /* medium */
    +screen(961, 1280)
        background-color #3f51b5

    /* large */
    +screen(1281, 1920)
        background-color #303f9f

    /* xlarge */
    +min-screen(1921)
        background-color #1a237e

```

## Mixins

Currently available mixins :
```styl

/* Screen sizes in dp*/
screen(minWidth, maxWidth)
screen-portrait(minWidth, maxWidth)
screen-landscape(minWidth, maxWidth)
min-screen(minWidth)
min-screen-portrait(minWidth)
min-screen-landscape(minWidth)
max-screen(maxWidth)
max-screen-portrait(maxWidth)
max-screen-landscape(maxWidth)

/* Size in pixels */
dp(density)

```

## License

MIT
